import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { GithubIssuesModule } from './components/github-issues/github-issues.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GithubIssuesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
