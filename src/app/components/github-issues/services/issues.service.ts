import { Injectable, EventEmitter } from '@angular/core'
import { Http, Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
 
import { Issue } from "./../classes/issue";

import { IssuesStatus } from "./../enums/issues-status.enum";

@Injectable()
export class IssuesService {

  allIssues: Issue[] = [];
  selectedIssues: Issue[] = [];
  $selectedIssuesUpdate = new EventEmitter<Issue[]>();
  selectedStatus: IssuesStatus;  
  private _issuessUrl: string = "./apidata/issues.json";
//  private _issuessUrl: string = "https://api.myjson.com/bins/rztjl";
  
  constructor(private _http: Http) { }
  
  getAllIssues(): Observable<Issue[]> {
    return this._http.get(this._issuessUrl)
      .map( (response: Response) => response.json() )    
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 400) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 409) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 406) {
          return Observable.throw(new Error(error.status));
        }
      } );
  }
  setAllIssues(issues: Issue[]): void { 
    this.allIssues = issues;
  }

  getSelectedIssues(): Issue[] {
    return this.selectedIssues;
  }
  setSelectedIssues(issues: Issue[]): void {
    this.selectedIssues = issues;
    this.$selectedIssuesUpdate.emit(issues);
  }
  
  getSelectedStatus(): IssuesStatus {
    return this.selectedStatus;
  }
  setSelectedStatus(selectedStatus: IssuesStatus): void {
    this.selectedStatus = selectedStatus;
  }
  
  changeIssueStatus(issue: Issue): void {
    let allIssues: Issue[] = this.allIssues;
    allIssues = allIssues.filter((x: Issue) => x !== issue);
  
    allIssues.push( new Issue(issue.date, issue.title, !issue.open) );
    this.setAllIssues(allIssues);
    this.setFilteredIssues(this.selectedStatus);
  }
  
  getFilteredIssues(issuesStatus: IssuesStatus): Issue[] {
    let selectedIssues: Issue[] = this.allIssues;
    switch(issuesStatus) {
      case IssuesStatus.CLOSED: {
        selectedIssues = this.allIssues.filter(
          (x: Issue) => x.open
        ) 
      }; break;
      case IssuesStatus.OPEN: {
        selectedIssues = this.allIssues.filter(
          (x: Issue) => !x.open 
        )
      }; break;
      case IssuesStatus.ALL: { selectedIssues = this.allIssues }; break;
    }
    return selectedIssues; 
  }
  setFilteredIssues(issuesStatus: IssuesStatus): void {
    this.setSelectedIssues(this.getFilteredIssues(issuesStatus));
  }
 
  
}