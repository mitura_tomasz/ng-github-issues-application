export class Issue {
  
  constructor(
    public date: string,
    public title: string,
    public open: boolean
  ) {}
  
}
