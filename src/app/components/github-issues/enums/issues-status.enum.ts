export enum IssuesStatus {
    CLOSED = "closed",
    OPEN = "open",
    ALL = "all"
}
