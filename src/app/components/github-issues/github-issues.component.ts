import { Component, OnInit, Input } from '@angular/core';

import { IssuesService } from "./services/issues.service";

import { Issue } from "./classes/Issue";

import { IssuesStatus } from "./enums/issues-status.enum";


@Component({
  selector: 'app-github-issues',
  templateUrl: './github-issues.component.html',
  styleUrls: ['./github-issues.component.scss']
})
export class GithubIssuesComponent implements OnInit {

  issues: Issue[];
  dates: string[];

  constructor(private _issuesService: IssuesService) { }

  ngOnInit() {    
    this._issuesService.getAllIssues().subscribe( 
      responseIssuesData => {
        this.issues = this.sortIssuesByTitle(responseIssuesData);
        this.dates = this.getDatesFromIssues(responseIssuesData);
        
        this._issuesService.setAllIssues(responseIssuesData);
        this._issuesService.setSelectedIssues(responseIssuesData);
        this._issuesService.setSelectedStatus(IssuesStatus.ALL);
        
        this._issuesService.$selectedIssuesUpdate.subscribe(
          (responseSelectedIssues: Issue[]) => {
            this.issues = this.sortIssuesByTitle(responseSelectedIssues)
            this.dates = this.getDatesFromIssues(responseSelectedIssues);
          }
        );
      }
    );
  }
  
  
  getDatesFromIssues(issues: Issue[]): string[] {
    let dates: string[] = [];
    issues.map((x: Issue) => {
      if (!dates.includes(x.date)) { dates.push(x.date) } 
    });
    return dates.sort().reverse();
  }
  
    sortIssuesByTitle(issues: Issue[]): Issue[] {
      return issues.sort(
        (issue1, issue2) => {
          const title1 = issue1.title.toLowerCase();
          const title2 = issue2.title.toLowerCase();
          if (title1 > title2) { return 1; }
          if (title1 < title2) { return -1; }
          return 0;
        }
      );
    }
}
