import { Component, OnInit, Input } from '@angular/core';

import { IssuesService } from "./../../services/issues.service";

import { Issue } from "./../../classes/Issue";

import { IssuesStatus } from "./../../enums/issues-status.enum";


@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.scss']
})
export class IssueComponent implements OnInit {

  @Input() issue: any;
  
  constructor(private _issuesService: IssuesService) { }

  ngOnInit() { }

  changeIssueStatus(): void {
    this._issuesService.changeIssueStatus(this.issue); 
  }
  
}
