import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemButtonsComponent } from './system-buttons.component';

describe('SystemButtonsComponent', () => {
  let component: SystemButtonsComponent;
  let fixture: ComponentFixture<SystemButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
