import { Component, OnInit } from '@angular/core';


import { IssuesStatus } from "./../../enums/issues-status.enum";

import { Issue } from "./../../classes/issue";

import { IssuesService } from "./../../services/issues.service";


@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  
  public issuesStatus: any = IssuesStatus;
  
  constructor(private _issuesService: IssuesService) { }

  ngOnInit() { }
  
  
  setIssuesStatus(status: IssuesStatus): void {
    this._issuesService.setSelectedStatus(status);
    this._issuesService.setFilteredIssues(status);
  } 
}
