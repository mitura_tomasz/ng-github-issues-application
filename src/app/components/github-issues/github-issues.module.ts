import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpModule } from '@angular/http';

import { IssuesService } from "./services/issues.service";

import { GithubIssuesComponent } from './github-issues.component';
import { SystemButtonsComponent } from './components/system-buttons/system-buttons.component';
import { FiltersComponent } from './components/filters/filters.component';
import { IssueComponent } from './components/issue/issue.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [
    GithubIssuesComponent,
    SystemButtonsComponent,
    FiltersComponent,
    IssueComponent
  ],
  providers: [
    IssuesService,
    DatePipe
  ],
  exports: [
    GithubIssuesComponent
  ]
})
export class GithubIssuesModule { }
